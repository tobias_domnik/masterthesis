# README #

Master-Thesis von Tobias Domnik zum Thema: Verfügbarkeit von Bio-Masse für eine Methanol-getrieben Elektromobilität

Deployed to: http://meoh-tool.appspot.com/

### How do I get set up without using git? ###

- Download zip via: https://bitbucket.org/tobias_domnik/masterthesis/get/814834fdb2d0.zip
- Execute meoh-tool.bat in root-directory
- load tool in browser with adress: localhost:8080

### How do I get set up? ###

- Download and install git
- git clone git@bitbucket.org:tobias_domnik/masterthesis.git
- Download and install npm 
- npm install ionic
- npm install http-server -g

### Start Application on mobile devices ###

* connect your device via adb (usb/wifi)
* Change directory to root-directory
* install app via: ionic run
App launches on mobile device


### Start Application on Browser ###

* Change directory to www
* Start server via: http-server
Site accesible in Browser at: localhost:8080/index


### Load App in Powerpoint ###
* Download and install LiveWeb Plugin for Powerpoint  (http://skp.mvps.org/liveweb.htm#.VRSZ0PnF858)
* Add [HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION] with decimal value: 11001 (for IE11 WebView)
* Enable Makros in Powerpoint and set lowest security configurations
* Start http-server like described above
* Include Website-Element with Plugin using http-Adress from above
App should load on the very page