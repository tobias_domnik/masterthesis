var http = require('http');
var express = require('express');

var PORT = process.env.PORT || 8080;
var server = express();
server.use(express.static(__dirname));

server.listen(PORT, function () {
    //Callback triggered when server is successfully listening. Hurray!
    console.log("Server listening on: http://localhost:%s", PORT);
    console.log("Server dirname", __dirname);
});