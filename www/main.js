require.config({
  waitSeconds: 100,
  paths: {
    'json': 'lib/requirejs-json/json',
    'text': 'lib/requirejs-text/text',
    'angular': 'lib/angular/angular',
    'angular-animate': 'lib/angular-animate/angular-animate',
    'angular-sanitize': 'lib/angular-sanitize/angular-sanitize',
    'angular-ui-router': 'lib/angular-ui-router/release/angular-ui-router',
    'ionic': 'lib/ionic/js/ionic',
    'ionic-angular': 'lib/ionic/js/ionic-angular',
    'bootstrap': 'lib/bootstrap/dist/js/bootstrap',
    'angular-bootstrap': 'lib/angular-bootstrap/ui-bootstrap',
    'angular-bootstrap-tpls': 'lib/angular-bootstrap/ui-bootstrap-tpls',
    'jQuery': 'lib/jquery/dist/jquery',
    'regression': 'lib/regression/src/regression',
    'raphael': 'lib/raphael/raphael',
    'morris': 'lib/morris/morris.min'
  },
  shim: {
    'angular': {
      'exports': 'angular'
    },
    'angular-animate': {
      deps: ['angular']
    },
    'angular-sanitize': {
      deps: ['angular']
    },
    'angular-ui-router': {
      deps: ['angular']
    },
    'ionic': {
      deps: ['angular', 'angular-animate', 'angular-sanitize', 'angular-ui-router'],
      exports: 'ionic'
    },
    'ionic-angular': {
      deps: ['angular', 'ionic']
    },
    'jQuery': {
      'exports': '$'
    },
    'bootstrap': {
      deps: ['jQuery']
    },
    'angular-bootstrap': {
      deps: ['jQuery', 'bootstrap', 'angular']
    },
    'angular-bootstrap-tpls': {
      deps: ['jQuery', 'bootstrap', 'angular']
    },
    'bootstrap-slider': {
      deps: ['angular', 'jQuery', 'bootstrap']
    },
    'raphael': {
      'exports': 'raphael',
      deps: ['jQuery']
    },
    'morris': {
      deps: ['jQuery', 'raphael']
    },
    'regression': {
      'exports': 'regression'
    }
  },
  priority: [
    'angular',
    'angular-ionic'
  ],
  text: {
    //Valid values are 'node', 'xhr', or 'rhino'
    env: 'xhr'
  },
  callback:function initialize () {
    'use strict';
    require([
      'angular',
      'ionic-angular',
      'jQuery',
      'bootstrap',
      'angular-bootstrap-tpls',
      'raphael',
      'morris',
      'regression',
      'app',
      'controllers',
      'services',
      'directives',
      'math'
    ], function (angular) {
      // init app
      console.log("SAM-Tool is booting");
      angular.element(document).ready(function () {
        angular.bootstrap(document, ['app']);
      });
    });
  }});