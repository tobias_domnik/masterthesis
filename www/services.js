define([
  'app',
  'json!./res/data.json'
], function (app, data) {
  'use strict';
  return angular.module('app.services', [])
    .service('dataService', function() {
      this.getData = function () {
        return cloneObject(data);
      };
      this.save = function (data) {
        localStorage.setItem('data', JSON.stringify(data));
        console.log("save data", JSON.parse(localStorage.getItem('data')));
      };
      this.restore = function (oldData) {
        if (localStorage.getItem('data')) {
          var savedData = localStorage.getItem('data');
          console.log("restore data");
          return JSON.parse(savedData);
        } else {
          return oldData;
        }
      }
    })

    .service('calcService', function() {
      var scenarioYears = ["2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"]
      var doCalc = function (data) {
        // For console logs
        var s = data.scenarios.map(function(scenario) {
          return scenario.name;
        });
        var logForScenario = s[0];

        // Method 1 pre-calculation
        data.method1.mileageGroups.forEach(function (mileageGroup) {
          mileageGroup["classAverage"] = (mileageGroup.upperBound + mileageGroup.lowerBound) / 2;
          mileageGroup["eDriveShare"] = eDriveShare(mileageGroup["classAverage"]);
          mileageGroup["nonElectricMileage"] = (mileageGroup["classAverage"] - (parseFloat(mileageGroup["eDriveShare"].toFixed(4)) * mileageGroup["classAverage"]));
          mileageGroup["electricMileage"] = (mileageGroup["classAverage"] - mileageGroup["nonElectricMileage"]);
          mileageGroup["totalMileage"] = mileageGroup["classAverage"];
          //console.log("nE: ",  mileageGroup["nonElectricMileage"], "E: ", mileageGroup["electricMileage"], "SUM: ",  mileageGroup["totalMileage"], " Test: ",  mileageGroup["totalMileage"] === (mileageGroup["nonElectricMileage"] + mileageGroup["electricMileage"]));
        });
        // Method 2 pre-calculation
        data.method2.clusters.forEach(function (cluster) {
          cluster["nonElectricMileage"] = 0; // value[Days, MDD]
          cluster["electricMileage"] = 0; // value[Days, MDD]
          cluster["totalMileage"] = 0; // value[Days, MDD]
          cluster.values.forEach(function (value) {
            cluster["nonElectricMileage"] += (value[0] * Math.max(0, (value[1] - data.method2.R3)));
            cluster["electricMileage"] += (value[0] * Math.min(data.method2.R3, value[1]));
            cluster["totalMileage"] += (value[0] * value[1]);
            //console.log("nE: ", cluster["nonElectricMileage"], "E: ", cluster["electricMileage"], " SUM: ", cluster["totalMileage"], " Test: ",  cluster["totalMileage"] === cluster["nonElectricMileage"] +  cluster["electricMileage"]);
          });
        });

        // Convert $-Values to Euro
        data.paths.forEach(function(path) {
          path['transportCostsMaritimeEuro'] = inEuro(path.transportCostsMaritimeDollar,  data['$_TO_€']);
        });

        // Feedstock paths
        data.paths.forEach(function (path) {
          // MeOH CAPACITY
          path["MeOHCapacity"] = path.capacity * path.correctionFactor * path.efficiencyRate;
          path["transportCosts"] = path.transportCostOverland + path.transportCostsMaritimeEuro + (data.includeTHC ? path.transportHandlingCosts : 0);
          // Transport Costs and Transport Emissions
          path["transportEmissionsMaritime"] = path.transportLength * (1 - path.transportShareOverland) * data.EMISSION_CONSTANT_MARITIME;
          path["transportEmissionsOverland"] = path.transportLength * path.transportShareOverland * data.LABYRINTHFACTOR * data.EMISSION_CONSTANT_OVERLAND;
          path["transportEmissions"] = path["transportEmissionsMaritime"] + path["transportEmissionsOverland"];
          // Cumulated Values
          path["cumulatedCostsPerTon"] = path.productionCosts + path.transportCosts + path.infrastructureCosts;
          path["cumulatedEmissionsPerTon"] = (path.productionEmissionFeedstock / path.efficiencyRate) + path.productionEmission + path.transportEmissions + path.infrastructureEmissions;
          path["totalCosts"] = data.costsWeight * path["cumulatedCostsPerTon"] + data.emissionsWeight * (path["cumulatedEmissionsPerTon"] * data.EMISSION_COSTS_PER_KGCO2);

          // Norming cost and emissions (without weighting!)
          path["productionCostsShare"] = path.productionCosts / path.cumulatedCostsPerTon;
          path["transportationCostsMaritimeShare"] = path.transportCostsMaritimeEuro / path.cumulatedCostsPerTon;
          path["transportationCostsOverlandShare"] = path.transportCostOverland / path.cumulatedCostsPerTon;
          path["infrastructureCostsShare"] = path.infrastructureCosts / path.cumulatedCostsPerTon;

          path["feedstockEmissionsShare"] = (path.productionEmissionFeedstock / path.efficiencyRate) / path.cumulatedEmissionsPerTon;
          path["productionEmissionsShare"] = path.productionEmission / path.cumulatedEmissionsPerTon;
          path["transportEmissionsMaritimeShare"] = path.transportEmissionsMaritime  / path.cumulatedEmissionsPerTon;
          path["transportEmissionsOverlandShare"] = path.transportEmissionsOverland  / path.cumulatedEmissionsPerTon;
          path["infrastructureEmissionsShare"] = path.infrastructureEmissions /  path.cumulatedEmissionsPerTon;
        });
        data.paths.sort(function (a, b) {
          return a.totalCosts - b.totalCosts;
        });

        // Scenario calculation
        data.scenarios.forEach(function (scenario) {
          // POLYNOMIAL CALCULATION
          scenario["polynomial"] = regression('polynomial', scenario.values, data.polynomialGrade);

          //Prepare Data for View: REEV- AND REM2030-SHARE
          scenario['vehicleScenario'] = scenario.polynomial.points.map(function(point, index) {
            if (point[1] < 0) {
              point[1] = 0;
            }
            return {'index': index, 'Jahr': scenarioYears[index], 'Fahrzeuge': parseFloat(point[1].toFixed(0)), 'REEVs': parseFloat((point[1] * data.REEVShare).toFixed(0)), 'REM2030': parseFloat((point[1] * data.REEVShare * data.REM2030Share).toFixed(0))};
          });

          // METHOD 1
          scenario["cumulatedMileagesMethod1"] = scenario['vehicleScenario'].map(function (part) {
            var year =  part.index;
            var totalCars =  part.REM2030;
            var nonElectricMileage = 0;
            var electricMileage = 0;
            var totalMileage = 0;
            if (totalCars < 0) {
              return [year, 0];
            }
            data.method1.mileageGroups.forEach(function (mileageGroup) {
              nonElectricMileage += (totalCars * mileageGroup.share * mileageGroup.nonElectricMileage);
              electricMileage += (totalCars * mileageGroup.share * mileageGroup.electricMileage);
              totalMileage += (totalCars * mileageGroup.share * mileageGroup.totalMileage);
            });
            //console.log(scenario.name, " Year: ", year, " Non-Electric-km: ", nonElectricMileage.toFixed(0), " bei ", totalCars.toFixed(4), " Fahrzeugen");
            //console.log("Assert: ", parseFloat(totalMileage.toFixed(2)) === parseFloat((nonElectricMileage + electricMileage).toFixed(2)));
            return {
              'index': year,
              'Jahr': part.Jahr,
              'Fahrleistung': parseFloat(nonElectricMileage.toFixed(0)),
              'ElektrischeFahrleistung': parseFloat((electricMileage).toFixed(0)),
              'AbsoluteFahrleistung': parseFloat((totalMileage).toFixed(0))};
          });

          // METHOD 2
          scenario["cumulatedMileagesMethod2"] = scenario['vehicleScenario'].map(function (part) {
            var year =  part.index;
            var totalCars =  part.REM2030;
            var nonElectricMileage = 0;
            var electricMileage = 0;
            var totalMileage = 0;
            if (totalCars < 0) {
              return [year, 0];
            }
            data.method2.clusters.forEach(function (cluster) {
              nonElectricMileage += totalCars * cluster.share * cluster.nonElectricMileage;
              electricMileage += (totalCars * cluster.share * cluster.electricMileage);
              totalMileage += (totalCars * cluster.share * cluster.totalMileage);
            });
            //console.log("Assert: ", parseFloat(totalMileage.toFixed(2)) === parseFloat((nonElectricMileage + electricMileage).toFixed(2)));
            return {'index': year,
              'Jahr': part.Jahr,
              'Fahrleistung': parseFloat(nonElectricMileage.toFixed(0)),
              'ElektrischeFahrleistung': parseFloat((electricMileage).toFixed(0)),
              'AbsoluteFahrleistung':parseFloat((nonElectricMileage + electricMileage).toFixed(0))};
          });

          // VEHICLE AGING AND CONSUMPTION
          scenario['cumulatedConsumptionMethod1'] = [];
          scenario['cumulatedConsumptionMethod2'] = [];
          var vehicles = [];
          for (var t = 0; t <= 16; t++) {
            var sum = 0;
            for (var i = 0; i < t; i++) {
              sum += parseFloat(vehicles[i]) * vehicleAging(t - i);
            }
            vehicles[t] = scenario['vehicleScenario'][t].REM2030 - sum; // Neufahrzeuge zu Zeitpunkt t
            var specSum = 0;
            for (var j = 0; j <= t; j++) {
              specSum += parseFloat(vehicles[j]) * vehicleAging(t - j) * consumption(data.InitialMethanolConsumption, data.learningEffect, j);
            }
            var method1KG = (specSum * scenario["cumulatedMileagesMethod1"][t].Fahrleistung / (scenario['vehicleScenario'][t].REM2030 > 1 ? scenario['vehicleScenario'][t].REM2030 : 1));
            var method2KG = (specSum * scenario["cumulatedMileagesMethod2"][t].Fahrleistung / (scenario['vehicleScenario'][t].REM2030 > 1 ? scenario['vehicleScenario'][t].REM2030 : 1));
            scenario['cumulatedConsumptionMethod1'][t] = {
              'index': t,
              'Jahr': scenarioYears[t],
              'kg': parseFloat(method1KG.toFixed(0)),
              't':  parseFloat(meohLtoT(method1KG).toFixed(0))
            };
            scenario['cumulatedConsumptionMethod2'][t] = {
              'index': t,
              'Jahr': scenarioYears[t],
              'kg': parseFloat(method2KG.toFixed(0)),
              't':  parseFloat(meohLtoT(method2KG).toFixed(0))
            }
            ;
          }

          // PATH USAGE CALCULATION
          scenario["paths"] = [];
          scenario["pathsMethod1"] = [];
          scenario["pathsMethod2"] = [];
          if (scenario.name === logForScenario) {
            //console.log("Szenario-Name: ", logForScenario);
          }
          scenario['vehicleScenario'].forEach(function (part) {
            //console.log(" - - - - - - - ");
            var kapSum = 0;
            scenario["paths"][part.index] = [];
            scenario["pathsMethod1"][part.index] = {'Jahr': part.Jahr};
            scenario["pathsMethod2"][part.index] = {'Jahr': part.Jahr};
            var array = data.paths.slice();
            array.forEach(function (p) {
              var path = cloneObject(p);
              if (part.index > 16) {
                return;
              }
              path["capacityUsageM1"] = Math.max(0, Math.min(path.MeOHCapacity, (scenario['cumulatedConsumptionMethod1'][part.index].t - kapSum)));
              path["capacityUsageM2"] = Math.max(0, Math.min(path.MeOHCapacity, (scenario["cumulatedConsumptionMethod2"][part.index].t - kapSum)));
              kapSum += parseFloat(path.MeOHCapacity);
              scenario["pathsMethod1"][part.index][path.name + ':' + path.feedstockName] = path["capacityUsageM1"];
              scenario["pathsMethod2"][part.index][path.name + ':' + path.feedstockName] = path["capacityUsageM2"];
              scenario["paths"][part.index].push(path);
              if (scenario.name === logForScenario && path["capacityUsageM1"] > 0) {
                // console.log(part.index,": Year: ", part.Jahr, " Fz: ", part.Fahrzeuge, " Bedarf: ", scenario['cumulatedConsumptionMethod1'][part.index].t, "t ", path.name, ":", path.feedstockName, " Pfad-Kapazität: ", path.MeOHCapacity, " Auslastung: ", path["capacityUsageM1"].toFixed(0), " = ", ((path["capacityUsageM1"]/path.MeOHCapacity)*100).toFixed(0), " %");
              }
            })
          });
          scenario["pathNames"] = Object.keys(scenario["pathsMethod1"][16]);
          scenario["pathNames"].shift();

          // NORMING COSTS AND EMISSIONS

          scenario["pathsMethod1"].forEach(function(pathsInYear, index) {
            // Capacity
            pathsInYear["demand"] = 0;
            // Costs
            pathsInYear["cumulatedCosts"] = 0;
            pathsInYear["cumulatedProductionCosts"] = 0;
            pathsInYear["cumulatedTransportationCostsMaritime"] = 0;
            pathsInYear["cumulatedTransportationCostsOverland"] = 0;
            pathsInYear["cumulatedInfrastructureCosts"] = 0;
            // Emissions
            pathsInYear["cumulatedEmissions"] = 0;
            pathsInYear["cumulatedFeedstockEmissions"] = 0;
            pathsInYear["cumulatedProductionEmissions"] = 0;
            pathsInYear["cumulatedTransportEmissionsMaritime"] = 0;
            pathsInYear["cumulatedTransportEmissionsOverland"] = 0;
            pathsInYear["cumulatedInfrastructureEmissions"] = 0;
            // Total Costs
            pathsInYear["cumulatedTotalCosts"] = 0;

            scenario.paths[index].forEach(function(path) {
              pathsInYear["demand"] += path["capacityUsageM1"];

              pathsInYear["cumulatedCosts"] += (path["capacityUsageM1"] * path["cumulatedCostsPerTon"]);
              pathsInYear["cumulatedProductionCosts"] += (path["capacityUsageM1"] * path.cumulatedCostsPerTon * path["productionCostsShare"]);
              pathsInYear["cumulatedTransportationCostsMaritime"] += (path["capacityUsageM1"] * path.cumulatedCostsPerTon * path["transportationCostsMaritimeShare"]);
              pathsInYear["cumulatedTransportationCostsOverland"] += (path["capacityUsageM1"] * path.cumulatedCostsPerTon * path["transportationCostsOverlandShare"]);
              pathsInYear["cumulatedInfrastructureCosts"] += (path["capacityUsageM1"] * path.cumulatedCostsPerTon * path["infrastructureCostsShare"]);

              pathsInYear["cumulatedEmissions"] += (path["capacityUsageM1"] * path.cumulatedEmissionsPerTon);
              pathsInYear["cumulatedFeedstockEmissions"] += (path["capacityUsageM1"] * path.cumulatedEmissionsPerTon * path["feedstockEmissionsShare"]);
              pathsInYear["cumulatedProductionEmissions"] += (path["capacityUsageM1"] * path.cumulatedEmissionsPerTon * path["productionEmissionsShare"]);
              pathsInYear["cumulatedTransportEmissionsMaritime"] += (path["capacityUsageM1"] * path.cumulatedEmissionsPerTon * path["transportEmissionsMaritimeShare"]);
              pathsInYear["cumulatedTransportEmissionsOverland"] += (path["capacityUsageM1"] * path.cumulatedEmissionsPerTon * path["transportEmissionsOverlandShare"]);
              pathsInYear["cumulatedInfrastructureEmissions"] += (path["capacityUsageM1"] * path.cumulatedEmissionsPerTon * path["infrastructureEmissionsShare"]);

              pathsInYear["cumulatedTotalCosts"] += (path["capacityUsageM1"] * (data.costsWeight * path.cumulatedCostsPerTon + data.emissionsWeight * path.cumulatedEmissionsPerTon * data['EMISSION_COSTS_PER_KGCO2']));
            });

            pathsInYear["normedCosts"] = [
              {'label': 'Produktion', 'value': parseFloat((pathsInYear.cumulatedProductionCosts/pathsInYear.cumulatedCosts).toFixed(3))},
              {'label': 'Transport Maritim', 'value': parseFloat((pathsInYear.cumulatedTransportationCostsMaritime/pathsInYear.cumulatedCosts).toFixed(3))},
              {'label': 'Transport Überland', 'value': parseFloat((pathsInYear.cumulatedTransportationCostsOverland/pathsInYear.cumulatedCosts).toFixed(3))},
              {'label': 'Infrastruktur', 'value': parseFloat((pathsInYear.cumulatedInfrastructureCosts/pathsInYear.cumulatedCosts).toFixed(3))}
            ];

            pathsInYear["normedEmissions"] = [
              {'label': 'Rohstoffe', 'value': parseFloat((pathsInYear.cumulatedFeedstockEmissions/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Produktion', 'value': parseFloat((pathsInYear.cumulatedProductionEmissions/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Transport Maritim', 'value': parseFloat((pathsInYear.cumulatedTransportEmissionsMaritime/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Transport Überland', 'value': parseFloat((pathsInYear.cumulatedTransportEmissionsOverland/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Infrastruktur', 'value': parseFloat((pathsInYear.cumulatedInfrastructureEmissions/pathsInYear.cumulatedEmissions).toFixed(3))}
            ];

            pathsInYear["normedTotalCosts"] = [
              {'label': 'Produktionskosten', 'value': parseFloat((data.costsWeight * pathsInYear.cumulatedProductionCosts/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportkosten (M)', 'value': parseFloat((data.costsWeight *  pathsInYear.cumulatedTransportationCostsMaritime/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportkosten (L)', 'value': parseFloat((data.costsWeight *  pathsInYear.cumulatedTransportationCostsOverland/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Infrastrukturkosten', 'value': parseFloat((data.costsWeight * pathsInYear.cumulatedInfrastructureCosts/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Emissionen Rohstoffbeschaffung', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedFeedstockEmissions * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Produktionsemissionen', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedProductionEmissions * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportemissionen (M)', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedTransportEmissionsMaritime * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportemissionen (L)', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedTransportEmissionsOverland * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Infrastrukturemissionen', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedInfrastructureEmissions * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))}
            ];
            if (pathsInYear.Jahr === '2030') {
              //console.log(pathsInYear);
              //console.log("Emissions Test: ", pathsInYear["cumulatedEmissions"], ' === ',  (pathsInYear["cumulatedFeedstockEmissions"] + pathsInYear["cumulatedProductionEmissions"] + pathsInYear["cumulatedTransportEmissionsMaritime"] + pathsInYear["cumulatedTransportEmissionsOverland"] + pathsInYear["cumulatedInfrastructureEmissions"]));
              //console.log("Cost Test: ", pathsInYear["cumulatedCosts"] , ' === ', (pathsInYear["cumulatedProductionCosts"] + pathsInYear["cumulatedTransportationCostsMaritime"] + pathsInYear["cumulatedTransportationCostsOverland"] + pathsInYear["cumulatedInfrastructureCosts"]));
              //console.log("Total Costs Test: ", pathsInYear["cumulatedTotalCosts"], ' === ', data.costsWeight * (pathsInYear["cumulatedProductionCosts"] + pathsInYear["cumulatedTransportationCostsMaritime"] + pathsInYear["cumulatedTransportationCostsOverland"] + pathsInYear["cumulatedInfrastructureCosts"]) + data.emissionsWeight * (pathsInYear["cumulatedEmissions"] * data.EMISSION_COSTS_PER_KGCO2));
            }
          });

          scenario["pathsMethod2"].forEach(function(pathsInYear, index) {
            // Capacity
            pathsInYear["demand"] = 0;
            // Costs
            pathsInYear["cumulatedCosts"] = 0;
            pathsInYear["cumulatedProductionCosts"] = 0;
            pathsInYear["cumulatedTransportationCostsMaritime"] = 0;
            pathsInYear["cumulatedTransportationCostsOverland"] = 0;
            pathsInYear["cumulatedInfrastructureCosts"] = 0;
            // Emissions
            pathsInYear["cumulatedEmissions"] = 0;
            pathsInYear["cumulatedFeedstockEmissions"] = 0;
            pathsInYear["cumulatedProductionEmissions"] = 0;
            pathsInYear["cumulatedTransportEmissionsMaritime"] = 0;
            pathsInYear["cumulatedTransportEmissionsOverland"] = 0;
            pathsInYear["cumulatedInfrastructureEmissions"] = 0;
            // Total Costs
            pathsInYear["cumulatedTotalCosts"] = 0;

            scenario.paths[index].forEach(function(path) {
              pathsInYear["demand"] += path["capacityUsageM2"];

              pathsInYear["cumulatedCosts"] += (path["capacityUsageM2"] * path["cumulatedCostsPerTon"]);
              pathsInYear["cumulatedProductionCosts"] += (path["capacityUsageM2"] * path.cumulatedCostsPerTon * path["productionCostsShare"]);
              pathsInYear["cumulatedTransportationCostsMaritime"] += (path["capacityUsageM2"] * path.cumulatedCostsPerTon * path["transportationCostsMaritimeShare"]);
              pathsInYear["cumulatedTransportationCostsOverland"] += (path["capacityUsageM2"] * path.cumulatedCostsPerTon * path["transportationCostsOverlandShare"]);
              pathsInYear["cumulatedInfrastructureCosts"] += (path["capacityUsageM2"] * path.cumulatedCostsPerTon * path["infrastructureCostsShare"]);

              pathsInYear["cumulatedEmissions"] += (path["capacityUsageM2"] * path.cumulatedEmissionsPerTon);
              pathsInYear["cumulatedFeedstockEmissions"] += (path["capacityUsageM2"] * path.cumulatedEmissionsPerTon * path["feedstockEmissionsShare"]);
              pathsInYear["cumulatedProductionEmissions"] += (path["capacityUsageM2"] * path.cumulatedEmissionsPerTon * path["productionEmissionsShare"]);
              pathsInYear["cumulatedTransportEmissionsMaritime"] += (path["capacityUsageM2"] * path.cumulatedEmissionsPerTon * path["transportEmissionsMaritimeShare"]);
              pathsInYear["cumulatedTransportEmissionsOverland"] += (path["capacityUsageM2"] * path.cumulatedEmissionsPerTon * path["transportEmissionsOverlandShare"]);
              pathsInYear["cumulatedInfrastructureEmissions"] += (path["capacityUsageM2"] * path.cumulatedEmissionsPerTon * path["infrastructureEmissionsShare"]);

              pathsInYear["cumulatedTotalCosts"] += (path["capacityUsageM2"] * (data.costsWeight * path.cumulatedCostsPerTon + data.emissionsWeight * path.cumulatedEmissionsPerTon * data['EMISSION_COSTS_PER_KGCO2']));
            });

            pathsInYear["normedCosts"] = [
              {'label': 'Produktion', 'value': parseFloat((pathsInYear.cumulatedProductionCosts/pathsInYear.cumulatedCosts).toFixed(3))},
              {'label': 'Transport Maritim', 'value': parseFloat((pathsInYear.cumulatedTransportationCostsMaritime/pathsInYear.cumulatedCosts).toFixed(3))},
              {'label': 'Transport Überland', 'value': parseFloat((pathsInYear.cumulatedTransportationCostsOverland/pathsInYear.cumulatedCosts).toFixed(3))},
              {'label': 'Infrastruktur', 'value': parseFloat((pathsInYear.cumulatedInfrastructureCosts/pathsInYear.cumulatedCosts).toFixed(3))}
            ];

            pathsInYear["normedEmissions"] = [
              {'label': 'Rohstoffe', 'value': parseFloat((pathsInYear.cumulatedFeedstockEmissions/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Produktion', 'value': parseFloat((pathsInYear.cumulatedProductionEmissions/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Transport Maritim', 'value': parseFloat((pathsInYear.cumulatedTransportEmissionsMaritime/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Transport Überland', 'value': parseFloat((pathsInYear.cumulatedTransportEmissionsOverland/pathsInYear.cumulatedEmissions).toFixed(3))},
              {'label': 'Infrastruktur', 'value': parseFloat((pathsInYear.cumulatedInfrastructureEmissions/pathsInYear.cumulatedEmissions).toFixed(3))}
            ];

            pathsInYear["normedTotalCosts"] = [
              {'label': 'Produktionskosten', 'value': parseFloat((data.costsWeight * pathsInYear.cumulatedProductionCosts/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportkosten (M)', 'value': parseFloat((data.costsWeight *  pathsInYear.cumulatedTransportationCostsMaritime/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportkosten (L)', 'value': parseFloat((data.costsWeight *  pathsInYear.cumulatedTransportationCostsOverland/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Infrastrukturkosten', 'value': parseFloat((data.costsWeight * pathsInYear.cumulatedInfrastructureCosts/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Emissionen Rohstoffbeschaffung', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedFeedstockEmissions * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Produktionsemissionen', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedProductionEmissions * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportemissionen (M)', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedTransportEmissionsMaritime * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Transportemissionen (L)', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedTransportEmissionsOverland * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))},
              {'label': 'Infrastrukturemissionen', 'value': parseFloat(( data.emissionsWeight * pathsInYear.cumulatedInfrastructureEmissions * data['EMISSION_COSTS_PER_KGCO2']/pathsInYear.cumulatedTotalCosts).toFixed(3))}
            ];
          });
        });
      };

      return {
        doCalc: doCalc
      };
    });
});

