define([
  'app',
  'text!./templates/main.html',
  'text!./templates/scenarioOptions.html',
  'text!./templates/consumptionOptions.html',
  'text!./templates/paths.html',
  'text!./templates/results.html',
  'text!./templates/information.html'
], function (app, mainTpl, scenarioOptionsTpl, consumptionOptionsTpl, pathsTpl, resultsTpl, informationTpl) {
  'use strict';

  return angular.module('app.directives', [])

    .directive('appContent', function () {
      return {
        template: mainTpl
      }
    })
    .directive('scenarioOptions', function () {
      return {
        template: scenarioOptionsTpl
      }
    })
    .directive('consumptionOptions', function () {
      return {
        template: consumptionOptionsTpl
      }
    })
    .directive('results', function () {
      return {
        template: resultsTpl
      }
    })
    .directive('paths', function () {
      return {
        template: pathsTpl
      }
    })
    .directive('information', function () {
      return {
        template: informationTpl
      }
    })
    .directive('barChart', function ($timeout) {
      return {
        restrict: 'E',
        template: '<div id="chart"></div>',
        replace: false,
        scope: {
          tabSelection: '=',
          data: "=",
          xkey: "=",
          ykeys: "=",
          labels: "=",
          min: "=",
          max: "="
        },
        link: function ($scope, element, attrs) {
          //console.log("Directive Data", $scope.data);
          $scope.bar = new Morris.Bar({
            element: 'chart',
            data: $scope.data,
            xkey: $scope.xkey,
            ykeys: $scope.ykeys,
            labels: $scope.labels,
            min: $scope.min,
            max: $scope.max,
            stacked: true,
            parseTime: false,
            hideHover: true,
            gridTextSize: 16
          });

          $scope.$watchGroup(['tabSelection', 'data'], function() {
            console.log("redraw!!");
            $scope.bar.setData($scope.data);
            $timeout(function() {
              $scope.bar.redraw();
            }, 50);
          });
        }
      }
    })

    .directive('lineChart', function ($timeout) {
      return {
        restrict: 'E',
        template: '<div id="line"></div>',
        replace: false,
        scope: {
          tabSelection: '=',
          data: "=",
          xkey: "=",
          ykeys: "=",
          labels: "=",
          min: "=",
          max: "="
        },
        // observe and manipulate the DOM
        link: function ($scope, element, attrs) {
          //console.log("Directive Data", $scope.data);
          $scope.chart = new Morris.Line({
            element: 'line',
            data: $scope.data,
            xkey: $scope.xkey,
            ykeys: $scope.ykeys,
            labels: $scope.labels,
            min: $scope.min,
            max: $scope.max,
            parseTime: false,
            hideHover: true,
            gridTextSize: 16
          });

          $scope.$watchGroup(['tabSelection', 'data'], function() {
            console.log("redraw!!");
            $scope.chart.setData($scope.data);
            $timeout(function() {
              $scope.chart.redraw();
            }, 50);
          });
        }
      }
    })

    .directive('lineChartMileage', function ($timeout) {
      return {
        restrict: 'E',
        template: '<div id="lineMileage"></div>',
        replace: false,
        scope: {
          tabSelection: '=',
          data: "=",
          xkey: "=",
          ykeys: "=",
          labels: "=",
          min: "=",
          max: "="
        },
        // observe and manipulate the DOM
        link: function ($scope, element, attrs) {
          //console.log("Directive Data", $scope.data);
          $scope.chart = new Morris.Line({
            element: 'lineMileage',
            data: $scope.data,
            xkey: $scope.xkey,
            ykeys: $scope.ykeys,
            labels: $scope.labels,
            min: $scope.min,
            max: $scope.max,
            parseTime: false,
            hideHover: true,
            gridTextSize: 16
          });

          $scope.$watchGroup(['tabSelection', 'data'], function() {
            console.log("redraw!!");
            $scope.chart.setData($scope.data);
            $timeout(function() {
              $scope.chart.redraw();
            }, 50);
          });
        }
      }
    })

    .directive('areaChart', function ($timeout) {
      return {
        restrict: 'E',
        template: '<div id="area" ng-cloak></div>',
        replace: false,
        scope: {
          tabSelection: '=',
          data: "=",
          xkey: "=",
          ykeys: "=",
          labels: "=",
          min: "=",
          max: "="
        },
        // observe and manipulate the DOM
        link: function ($scope, element, attrs) {
          //console.log("Directive Data", $scope.data);
          $scope.chart = new Morris.Area({
            element: 'area',
            data: $scope.data,
            xkey: $scope.xkey,
            ykeys: $scope.ykeys,
            labels: $scope.labels,
            min: $scope.min,
            max: $scope.max,
            parseTime: false,
            hideHover: true,
            behaveLikeLine: true,
            gridTextSize: 16
          });

          $scope.$watchGroup(['tabSelection', 'data'], function() {
            //console.log("redraw!!");
            $scope.chart.setData($scope.data);
            $timeout(function() {
              $scope.chart.redraw();
            }, 50);
          });
        }
      }
    })

    .directive('donutChart', function ($timeout) {
      return {
        restrict: 'E',
        template: '<div id="donut" ng-cloak></div>',
        replace: false,
        scope: {
          tabSelection: '=',
          data: "=",
          labels: "="
        },
        // observe and manipulate the DOM
        link: function ($scope, element, attrs) {
          //console.log("Directive Data", $scope.data);
          function format (y, data) {
            return (y * 100).toFixed(1) + '%';
          }

          $scope.chart = new Morris.Donut({
            element: 'donut',
            data: $scope.data,
            formatter: format,
            resize: false
          });

          $scope.$watchGroup(['tabSelection', 'data'], function() {
            console.log("redraw!!");
            $scope.chart.setData($scope.data);
            $timeout(function() {
              $scope.chart.redraw();
            }, 50);
          });
        }
      }
    });
});



