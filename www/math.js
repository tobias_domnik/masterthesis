/**
 * Math helper Methods
 */
(function () {
  // Electric Drive Share for Method 1
  function eDriveShare(d) {
    return (0.99 * Math.exp(-2.1 * Math.pow(10, -5) * d));
  }
  // Vehicle Aging
  function vehicleAging(t) {
    return Math.exp(-Math.pow((t/14.1), 2.4));
  }
  // Vehicle BtL-consumption
  function consumption(initialValue, learningEffect, t) {
    return (initialValue/(1+learningEffect*t));
  }
  // MeOH l to t conversion
  function meohLtoT(liter) {
    return liter * 0.001 *0.79;
  }
  // MeOH T to l converstion
  function meohTtoL(tons) {
    return tons / 0.001 / 0.79;
  }
  // $ to €
  function inEuro(dollar, exchangeFactor) {
    return parseFloat(dollar) * exchangeFactor;
  }
  // t to km
  function perKm(dollar, exchangeFactor) {
    return parseFloat(dollar) * exchangeFactor;
  }

  // clone object
  function cloneObject(obj) {
    if (obj === null || typeof obj !== 'object') {
      return obj;
    }
    var temp = obj.constructor(); // give temp the original obj's constructor
    for (var key in obj) {
      temp[key] = cloneObject(obj[key]);
    }
    return temp;
  }
    window.eDriveShare = eDriveShare;
    window.vehicleAging = vehicleAging;
    window.consumption = consumption;
    window.meohLtoT = meohLtoT;
    window.meohTtoL = meohTtoL;
    window.inEuro = inEuro;
    window.cloneObject = cloneObject;
}());