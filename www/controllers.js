define([
  'app',
  'text!./templates/addScenario.html',
  'services',
  'directives'
], function (app, addScenarioTpl) {
  'use strict';

  angular.module('app.controllers', [])

    .controller('MainCtrl', ['$rootScope', '$scope', '$modal', 'dataService', 'calcService', function ($rootScope, $scope, $modal, dataService, calcService) {
      $scope.scenarioYears = ["2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"]
      $scope.scenarioIndex =  0;
      $scope.withElectricMileage = false;
      //$scope.scenario1 = $rootScope.data.scenarios[0];
      //$scope.scenario2 = $scope.data.scenarios[1]; // deprecated
      //$scope.scenario2Disabled = true; // deprecated
      $scope.methodNameMap = {
        Method1: "Jahresfahrleistung",
        Method2: "Tagesfahrleistung"
      };
      $scope.tabSelection = 'vehicleScenario';
      $scope.chosenMethod = "Method1";
      $scope.chosenYear = 0;
      $scope.chosenNorm = "normedCosts";
      $scope.norms = {
        'normedCosts': 'normierte Kosten',
        'normedEmissions': 'normierte Emissionen',
        'normedTotalCosts': 'normierte Gesamtkosten'
      };
      $scope.unit = 't';

      $scope.setScenarioIndex = function (index) {
        $scope.scenarioIndex = index;
      };

      $scope.setNorm = function (norm) {
        $scope.chosenNorm = norm;
      };

      $scope.setYear = function (year) {
        $scope.chosenYear = year;
      };

      $scope.newScenario = function () {
        var modalInstance = $modal.open({
          template: addScenarioTpl,
          controller: ['$rootScope', '$scope', '$modalInstance', 'calcService', function ($rootScope, $scope, $modalInstance, calcService) {
            $scope.scenarioYears = ["2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"]
            $scope.ok = function (newScenario) {
              var o = {};
              o['name'] = newScenario.name;
              var arr = [];
              Object.keys(newScenario.values).forEach(function(year) {
                arr.push([parseFloat(year), newScenario.values[year] ? parseFloat(newScenario.values[year]) : null]);
              });
              o['values'] = arr;
              $rootScope.data.scenarios.push(o);
              console.log($rootScope.data);
              calcService.doCalc($rootScope.data);
              $modalInstance.close();
            };
            $scope.cancel = function () {
              $modalInstance.close();
            };
          }]
        });
      };

      $scope.openMethod = function (method) {
        var methodNaming = {
          consumptionCalculation: 'Methodik zur Berechnung des Methanol-Bedarfs',
          feedstockPaths: 'Methodik zur Evaluation der Versorgungspfade'
        };
        var modalInstance = $modal.open({
          template: '<div class="modal-header">'+
          '<h3 class="modal-title">' + methodNaming[method] +'</h3></div>'+
          '<div class="modal-body imageWrapper">'+
          '<img ng-class="isMobile ? ' +  "'mobileImg'"  + ':' + "'desktopImg'" + '" src="www/img/' + method + '.JPG">' +
          '</div>' +
          '<div class="modal-footer">'+
          '<button class="btn btn-primary" ng-click="ok()">Ok</button>'+
          '</div>',
          size: 'lg',
          controller: ['$rootScope', '$scope', '$modalInstance', function ($rootScope, $scope, $modalInstance) {
            $scope.ok = function () {
              $modalInstance.close();
            };
          }]
        });
      };

      $scope.save = function () {
        dataService.save($scope.data);
        console.log('Daten gespeichert!');
      };

      $scope.restore = function () {
        $scope.data = $rootScope.data = dataService.restore();
        console.log('Daten wiederhergestellt!');
      };

      $scope.reset = function () {
        $scope.data = $rootScope.data =  dataService.getData();
        console.log('Daten zurückgesetzt!');
      };
    }])

    .filter('filterPaths', function () {
      return function (allPaths, pathsUsageInYear) {
        var result = [];
        allPaths.forEach(function(path) {
          if (pathsUsageInYear[path.name + ':' + path.feedstockName] > 0) {
            result.push(path);
          }
        });
        //console.log("Custom Filter: ", result);
        return result;
      };
    })

    .controller('AppCtrl', ['$rootScope', '$scope', 'dataService', 'calcService', function ($rootScope, $scope, dataService, calcService) {
      console.log("Scenario Assessment Tool starting");
      $scope.loaded = true;
      $scope.contentUrl = "www/templates/main.html";

      if (localStorage.getItem('data')) {
        $rootScope.data = JSON.parse(localStorage.getItem('data'));
      } else {
        $rootScope.data = dataService.getData();
        console.log("Basis data", $rootScope.data);
        calcService.doCalc($rootScope.data);
      }

      $scope.$watchCollection("data", function (data) {
        console.log("data changed, calculating!", data);
        if (data) {
          calcService.doCalc(data);
        }
      });

      // Zoom-Möglichkeit
      var zoomLevel = 1;
      var updateZoom = function(zoom) {
        zoomLevel += zoom;
        $('.appContent').css({ zoom: zoomLevel, '-moz-transform': 'scale(' + zoomLevel + ')' });
      };

      $('.zoomIn').on('click',function(){
        updateZoom(0.1);
      });

      $('.zoomOut').on('click',function(){
        updateZoom(-0.1);
      });
    }]);
});

