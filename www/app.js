define([
  'angular',
  'json!./res/data.json'
], function (angular, data) {
  'use strict';

  return angular.module('app', [
    'ionic',
    'app.controllers',
    'app.services',
    'app.directives',
    'ui.bootstrap'])
    .run(function ($ionicPlatform, $rootScope) {
      $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        ionic.Platform.fullScreen(true, true);
        ionic.Platform.isFullScreen = true;

        if (window.cordova && window.cordova.plugins.Keyboard) {
          cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
          StatusBar.hide();
        }

        var isMobile = {
          Android: function() {
            return navigator.userAgent.match(/Android/i);
          },
          BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
          },
          iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
          },
          Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
          },
          Windows: function() {
            return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
          },
          any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
          }
        };

        var isOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
        var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
        var isChrome = !!window.chrome && !isOpera;              // Chrome 1+
        var isIE = !!document.documentMode;   // At least IE6

        $rootScope.isMobile = isMobile.any() ? true : false;
        $rootScope.badBrowser = (isIE || isSafari);
        console.log("Browser is IE or Safari: ", $rootScope.badBrowser);

        console.log("Mobile mode: ", $rootScope.isMobile);
      });
    });
});
